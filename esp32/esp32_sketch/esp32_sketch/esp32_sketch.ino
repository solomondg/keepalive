char receivedChar;

void setup() {
  Serial.begin(115200);
  Serial.println("x");
  pinMode(25, INPUT);
  pinMode(26, INPUT);
}

void loop() {
  if (Serial.available() > 0) {
    receivedChar = Serial.read();
    Serial.println(receivedChar);
    if(receivedChar == '0')
      tap(25);
    else if(receivedChar == '1')
      tap(26);
  }
  delay(100);
}

void tap(int pin) {
  pinMode(pin, OUTPUT);
  digitalWrite(pin, LOW);
  delay(250);
  pinMode(pin, INPUT);
  digitalWrite(pin, LOW);
  delay(250);
}

