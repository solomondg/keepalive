from collections import namedtuple

Frame = namedtuple("Frame", "G X I D")


class Simulator:
    G = 150  # Blood glucose concentration (mg/dL)
    X = 0  # Effect of active insulin (1/min)
    I = 0  # Blood insulin concentration (mU/L)
    D = 0  # Meal disturbance function (mg/dL/min) (aka ingested glucose)
    Gsc = 0  # Subcutaneous glucose concentration (mg/dL) (idk why we care)
    U = 0  # Exogenous insulin (mU/min) (aka injected insulin)

    Ib = 0  # Basal blood insulin concentration (mU/L) -> Zero due to diabetes
    Vi = 1  # Volume of insulin distribution pool (L)

    t = 0  # Sim time, minutes

    # Glucose clearance rate independent of insulin (1/min) (removal via urine, use by brain)
    independent_glucose_clearance_rate = 0.0028735

    # Active insulin clearance rate (1/min) (blood insulin usage rate)
    active_insulin_clearance_rate = 0.028344

    # Increase in uptake ability caused by insulin (L/(min^2 mU))
    insulin_uptake_factor = 5.035E-4

    # Decay rate of blood insulin (1/min) (insulin degrades over time, even in the body)
    blood_insulin_decay = 0.01

    # Basal glucose release rate (mg/dL/min)
    basal_glucose_release = 1.0

    # Decay rate of a meal disturbance (1/min)
    meal_disturbance_decay = 0.0

    # Set the basal insulin rate (unit/minute)
    def set_basal_insulin_rate(self, basal: float):
        # this should modulate U -> let's only do basal, not bolus (more predictable/safe)
        pass

    def add_carbohydrates(self, carbs: float, record=True):
        # Should increment D, some tuning will be required to figure out a multiplication constant to make it
        # affect bgl realistically
        # 15 carbs should roughly correspond to a rise of 25-75 units - we cna tune later
        # Don't add this to a dosage history if record is false - aka random disturbances like unreported snacks
        pass

    def update(self, dt: float):
        # update G, X, I, D state variables
        # if dt is greater than a certain min dt, run update with a smaller dt until we get to that time
        dG = -(self.independent_glucose_clearance_rate + self.X) * self.G + self.D + self.basal_glucose_release
        dX = -self.active_insulin_clearance_rate * self.X + self.insulin_uptake_factor * (self.I - self.Ib)
        dI = -self.blood_insulin_decay * self.I + self.U / self.Vi
        dD = -self.meal_disturbance_decay * self.D

        self.G += dG * dt
        self.X += dX * dt
        self.I += dI * dt
        self.D += dD * dt

    def get_frame(self):
        return Frame(self.G, self.X, self.I, self.D)

    def get_basal_history(self, minutesBack: float):
        # Should return scipy interp1d of the basal rate (U) since minutesBack
        pass

    def get_carb_history(self, minutesBack: float):
        # Should return scipy interp1d of the _ON THE BOOK_ administered carbs (U) since minutesBack
        pass

    # Set the insulin effectiveness multiplier
    def set_insulin_effectiveness(self, multi: float):
        # Self explanitory, just multiply basal rate
        pass

    # Set the basal glucose rate multiplier
    def set_basal_glucose_rate(self, multi: float):
        # liver releases diff amounts of glucose over diff times of the day, we can gain schedule with this, should be able to modulate p5
        pass
