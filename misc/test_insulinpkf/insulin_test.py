import scipy as sp
import numpy as np
import scipy.integrate
import matplotlib.pyplot as plt
from pint import UnitRegistry

unit = UnitRegistry()

bgl = 150
isf = 1
dose = 1

# states
I = dose  # blood insulin
X = 0  # active insulin
G = bgl  # bgl

p1 = 0  # glucose clearance independant of insulin (urine, etc)
p2 = 0.1  # rate of clearance of active insulin (insulin go bye bye)
p3 = 0.5  # increase in glucose update caused by insulin (yay no hyperglycemia)
p4 = 1  # decay of blood insulin (sad face)

# the following parameters are 0 cause of the whole diabetes thing
p5 = 0  # target bgl level
p6 = 0  # rate of pancreatic release after glucose bolus

Gb = 0  # basal bgl concentration
Ib = 0  # basal insulin concentration


def slope_func(state, _):
    G, X, I = state
    dG = -(p1 + X) * G * isf + p1 * Gb
    dX = -p2 * X + p3 * (I - Ib)
    dI = p6 * (G - p5) - p4 * (I - Ib)

    return dG, dX, dI


y0 = [G, X, I]
t = np.linspace(0, 100, 100)

y = sp.integrate.odeint(slope_func, y0, t)

print(y)
#print(slope_func(y0, None))
G_dat = [i[0] for i in y]
X_dat = [i[0] for i in y]
I_dat = [i[0] for i in y]

plt.plot(t, G_dat)
plt.show()

