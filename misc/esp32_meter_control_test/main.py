import sys

import serial
import time

insulin_dose = .5

if len(sys.argv) > 1:
    insulin_dose = float(sys.argv[1])


def startup_sequence(ser_: serial.Serial):
    print("Startup")
    ser_.write(b'0')  # Initial wakeup button press
    time.sleep(10)
    ser_.write(b'0')  # Main menu
    time.sleep(1)
    ser_.write(b'0')  # Bolus
    time.sleep(5)


def set_dose(ser_: serial.Serial, insulin_dose: float):
    print("Total insulin dose: " + str(insulin_dose))
    doses = int(insulin_dose / .05)
    print("Taps: " + str(doses))
    for i in range(doses):
        ser_.write(b'1')
        time.sleep(.2)


def inject(ser_: serial.Serial):
    print("Injecting")
    ser_.write(b'0')
    time.sleep(.5)
    ser_.write(b'0')


with serial.Serial('/dev/ttyUSB0', 115200) as ser:
    startup_sequence(ser)
    set_dose(ser, insulin_dose)
    inject(ser)

print("Finished")

