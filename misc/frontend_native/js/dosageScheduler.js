var childProcess = require('child_process');

console.log("Loaded dosage scheduling plugin");

function handleDosageClick(e) {
    console.log("Handling create click");

    var form = document.dosageForm;
    var dosage = form.dosage.value;

    console.log("Dosage: " + dosage);

    let py = childProcess.spawn('python3',['../esp32_meter_control_test/main.py', parseFloat(dosage)]);

    py.stdout.on('data', data => console.log('data : ', data.toString()));
    py.on('error', (err) => {
        console.log('Failed to start process',err);
    });
    py.on('close', ()=>{
        console.log('Python process closed')
    });

    return false;
}
