var ctx;
var bloodGlucoseChart;

options = {
    responsive: true,
    title: {
        display: true,
        text: 'Blood Glucose Monitor'
    },
    tooltips: {
        mode: 'index',
        intersect: false,
    },
    hover: {
        mode: 'nearest',
        intersect: true
    },
    scales: {
        xAxes: [{
            display: true,
            scaleLabel: {
                display: true,
                labelString: 'Time (minutes ago)'
            }
        }],
        yAxes: [{
            display: true,
            scaleLabel: {
                display: true,
                labelString: 'Blood Glucose (mg/dL)'
            },
            id: 'bloodGlucose'
        }, {
            display: true,
            scaleLabel: {
                display: true,
                labelString: 'Insulin Dosage (U)'
            },
            id: 'insulinDosage'
        }]
    }
};

data = {
    labels: [],
    datasets: [{
        label: 'Blood Glucose',
        data: [],
        pointBackgroundColor: [],
        pointBorderColor: 'black',
        borderColor: 'rgb(130, 177, 255)',
        fill: false,
        yAxisID: 'bloodGlucose'
    }, {
        label: 'Insulin Dosage',
        data: [],
        borderColor: 'rgba(239, 197, 146, .3)',
        backgroundColor: 'rgba(239, 197, 146, .3)',
        pointRadius: 0,
        fill: false,
        yAxisID: 'insulinDosage'
    }, {
        label: "Suggested Insulin Dosage",
        data: [],
        borderColor: 'rgba(239, 150, 125, .4)',
        backgroundColor: 'rgba(239, 150, 125, .4)',
        pointRadius: 0,
        fill: false,
        yAxisID: 'insulinDosage'
    }, {
        label: 'Glucose In',
        data: [],
        borderColor: 'rgba(50, 255, 100, .4)',
        backgroundColor: 'rgba(50, 255, 100, .4)',
        fill: false
    }]
};

function interpretBGLData(data_) {
    let interpreted_data = [];
    let timestamps = [];
    let colors = [];

    let len = data_.length;
    for (let i = 0; i < len; i++) {
        let bgl = data_[i]["bgl"];
        interpreted_data.push(bgl);
        timestamps.push((len - i - 1) * -5);
        colors.push(bgl < 120 ? 'rgba(255,0,0,.6)' : (
            bgl > 180 ? 'rgba(255,255,0,.6)' :
                'rgba(0,0,255,.6)'
        ));
    }
    data.labels = timestamps;
    data.datasets[0].data = interpreted_data;
    // data.datasets[0].pointBorderColor = colors;
    data.datasets[0].pointBackgroundColor = colors;
    console.log(interpreted_data);
}

function interpretInsulinData(data_) {
    let interpreted_data = [];
    let timestamps = [];
    let current_time = Date.now();

    let len = data_.length;
    for (let i = 0; i < len; i++) {
        interpreted_data.push(data_[i]["insulin"]);
        let delta_time = (current_time - data_[i]["time"]) / 60; // Minutes delta
        timestamps.push(Math.floor(delta_time / 5) * -5);
    }
    data.datasets[1].data = interpreted_data;
    data.datasets[1].labels = timestamps;
    console.log(interpreted_data);
}

function setupPlot() {
    ctx = $("#bloodGlucoseChart");
    bloodGlucoseChart = new Chart(ctx, {
        type: 'line',
        data: data,
        options: options
    });
}

function onLoad() {
    $.get('http://104.196.132.224/bgl', function (data_, status_) {
        console.log(data_);
        interpretBGLData(data_);
        setupPlot();
    });
    $.get('http://104.196.132.224/insulin', function (data_, status_) {
        console.log(data_);
        interpretInsulinData(data_);
        if(bloodGlucoseChart != null) bloodGlucoseChart.update();
    });


}