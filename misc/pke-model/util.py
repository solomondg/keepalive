def slope(input_x: list, input_y: list):
    output = []
    for i in range(1, len(input_x)):
        output.append(instantaneous_slope(input_x[i - 1], input_y[i - 1], input_x[i], input_y[i]))
    return output


def instantaneous_slope(x1, y1, x2, y2):
    return (y2 - y1) / (x2 - x1)
