import numpy as np

from model import i_f, Frame
import matplotlib.pyplot as plt

from util import slope

I_0 = 1
S_0 = 0
B_0 = 150

time = np.linspace(0, 600, 10000)
frames = [Frame(I_0, I_0 * .9435)]

delta_x = time[1] - time[0]

# Run simulation
for index in range(1, len(time)):
    last_frame = frames[index - 1]
    (i_next, di_next) = i_f(last_frame.i, last_frame.di, delta_x)
    # s_next = s_f(last_frame.s, i_next, delta_x)
    # b_next = b_f(last_frame.b, s_next, delta_x)
    frames.append(Frame(i_next, di_next))

# Find the onset time (5% of insulin has been used)
# https://stackoverflow.com/questions/9706041/finding-index-of-an-item-closest-to-the-value-in-a-list-thats-not-entirely-sort
# /shrug, it works. Idk how.
onset_threshold = .95
onset_index = min(range(len(frames)), key=lambda i: abs(frames[i].i - onset_threshold))
print("Onset: Index: " + str(onset_index) + ", Time (m): " + str(time[onset_index]) +
      ", Value: " + str(frames[onset_index].i))

i_slope = slope(time, Frame.get_i_list(frames))

# plt.plot(time[1:], i_slope)
# plt.show()
plt.plot(time, Frame.get_i_list(frames))
# plt.plot(time, Frame.get_s_list(frames))
plt.show()
# plt.plot(time, Frame.get_b_list(frames))
# plt.show()
