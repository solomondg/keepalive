

# I = 51.51 e^(-0.007049 x) - 45.37 e^(-0.1773 x)
# I'' + .184349I' + .0012457866I = 0
# I(0) = 8.14, I'(0) = 7.681006
# Ratio: 7.68/8.14 = 94.35%
def i_f(i_prev, di_prev, dt):
    ddi = -.184 * di_prev - .00125 * i_prev
    di = di_prev + ddi * dt
    return i_prev + di * dt, di


class Frame:
    def __init__(self, i, di):
        self.i = i
        self.di = di

    @staticmethod
    def get_i_list(frame_list: list):
        return list(map(lambda frame: frame.i, frame_list))
