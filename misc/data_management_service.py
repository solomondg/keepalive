#!/usr/bin/env python3

from flask import Flask, jsonify
from flask import request
import json

from datetime import timedelta
from flask import make_response, request, current_app
from functools import update_wrapper


def varg():
    global bgl
    global insulin


varg.bgl = [{"time": 0, "bgl": 0}]
varg.insulin = [{"time": 0, "insulin": 0, "shouldTrigger": False}]

app = Flask(__name__)


def crossdomain(origin=None, methods=None, headers=None,
                max_age=21600, attach_to_all=True,
                automatic_options=True):
    if methods is not None:
        methods = ', '.join(sorted(x.upper() for x in methods))
    if headers is not None and not isinstance(headers, str):
        headers = ', '.join(x.upper() for x in headers)
    if not isinstance(origin, str):
        origin = ', '.join(origin)
    if isinstance(max_age, timedelta):
        max_age = max_age.total_seconds()

    def get_methods():
        if methods is not None:
            return methods

        options_resp = current_app.make_default_options_response()
        return options_resp.headers['allow']

    def decorator(f):
        def wrapped_function(*args, **kwargs):
            if automatic_options and request.method == 'OPTIONS':
                resp = current_app.make_default_options_response()
            else:
                resp = make_response(f(*args, **kwargs))
            if not attach_to_all and request.method != 'OPTIONS':
                return resp

            h = resp.headers

            h['Access-Control-Allow-Origin'] = origin
            h['Access-Control-Allow-Methods'] = get_methods()
            h['Access-Control-Max-Age'] = str(max_age)
            if headers is not None:
                h['Access-Control-Allow-Headers'] = headers
            return resp

        f.provide_automatic_options = False
        return update_wrapper(wrapped_function, f)

    return decorator


@app.route('/bgl', methods=['GET'])
@crossdomain(origin='*')
def get_bgl():
    return jsonify(varg.bgl)


@app.route('/bgl', methods=['PUT'])
@crossdomain(origin='*')
def put_bgl():
    js = request.json
    print("json ", js)
    varg.bgl = js
    return jsonify(varg.bgl)


@app.route('/insulin', methods=['GET'])
@crossdomain(origin='*')
def get_insulin():
    return jsonify(varg.insulin)


@app.route('/insulin', methods=['PUT'])
@crossdomain(origin='*')
def put_insulin():
    js = request.json
    print("json ", js)
    varg.insulin = js
    return jsonify(varg.insulin)


@app.route('/insulin', methods=['POST'])
@crossdomain(origin="*")
def post_insulin():
    js = request.json
    print("json ", js)
    print(request)
    varg.insulin.append(js)
    return jsonify(dict(httpCode=200))


if __name__ == '__main__':
    app.run(debug=True)
