from typing import List

meterSamplingPeriod = 60.0*5 # Dexcom samples every 5 minutes
minDisplayLow = 40 # min value dexcom detects
maxDisplayHigh = 400 # max value dexcom detects
sensorWarmupPeriod = 60*60*2 # 2h warmup


