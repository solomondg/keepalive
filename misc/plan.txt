Tiers/Milestones:
    1. 
        a. Pharmaceutokinetic endocrine model
        b. Prediction net (worldmodels)
        c. Control net (worldmodels)

    2. 
        a. Dexcom integration
        b. Web BGL display
        c. Prediction display
        d. Recommended action display

    3. 
        a. Pump integration (either RF or meter hacking)
        b. Safety subsystems
