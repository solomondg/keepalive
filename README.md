### KeepAlive Artificial Pancreas System


KeepAlive is a full-featured machine learning-powered artificial pancreas system for patients with Type 1 Diabetes.

It provides closed loop control through continuous glucose monitor (CGM) readings and automated insulin pump doses.

A Dragonboard 410C accesses blood glucose data from a Dexcom G6 CGM. Then, it is uploaded to a Google Cloud Platform compute server for processing and machine learning-based dosage suggestion and evaluation. The Dragonboard then downloads the dosage data and sends it to an ESP32, which is connected to the keypads of an Animas Ping insulin pump remote control. The control then commands the Ping to administer the specified amount of insulin, completing the closed loop cycle.

A deep neural network was trained on a pharmacokinetic endocrine simulator using DDQN-based (Double Deep Q-Network) reinforcement learning techniques. It is able to safely regulate blood glucose by modulating the basal, or background, insulin rate in order to compensate for carbohydrate intake and the natural glycogen release cycle of the liver.

Overall, this system provides a robust and effective artificial pancreas solution for Type 1 Diabetics, helping alleviate the burden of glucose self-management and reducing long and short-term disease complications.
