import scipy as sp
import numpy as np
import scipy.integrate
import matplotlib.pyplot as plt
from pint import UnitRegistry

unit = UnitRegistry()

unit.define('unit = 0.01 * milliliter = U')

bgl = 150
isf = 1
dose = 1

# states
G = 150  # blood glucose concentration (mg/dL)
X = 0  # effect of active insulin (1/min)
I = 0  # blood insulin concentration (mU/L)
D = 0  # meal disturbance function (mg/dL/min) (aka ingested glucose)
Gsc = 0  # subcutanious glucose concentration (mg/dL) (idk why we care)
U = 0  # exogenous insulin (mU/min) (aka injected insulin)

Gb = 200  # basal blood glucose concentration (mg/dL) (nonzero -> liver releases shit, changes based on time of day)
Ib = 0  # basal blood insulin concentration (mU/L) (zero cause diabetes)

Vi = 1  # volume of insulin distribution pool (L)

p1 = 0.0028735  # glucose clearance rate independent of insulin (1/min) (essentially shit peed out and used by the brain
p2 = 0.028344  # rate of clearance of active insulin (1/min) (the rate that blood insulin is used up/taken in by cells and shit)
p3 = 5.035E-4  # increase in uptake ability caused by insulin (L/(min^2 mU)) ()
p4 = 0.01  # decay rate of blood insulin (1/min) (insulin degrades over time, even in the body)
drate = 0  # decay rate of the meal disturbance (1/min) (sort of how fast the glucose is absorbed)
p5 = 1.0 # basal glucose release rate (mg/dL/min)

def slope_func(state, t):
    if t <= 1:
        U = 1
    else:
        U = 0.001

    G, X, I, D = state
    #dG = -(p1 + X) * G + p1 * Gb + D    # Old style
    dG = -(p1 + X) * G + D + p5
    dX = -p2 * X + p3 * (I - Ib)
    dI = -p4 * I + U / Vi
    dD = -drate * D

    return dG, dX, dI, dD


y0 = [G, X, I, D]
t = np.linspace(0, 3000, 100)

y = sp.integrate.odeint(slope_func, y0, t)

print(y)
# print(slope_func(y0, None))
G_dat = [i[0] for i in y]
X_dat = [i[1] for i in y]
I_dat = [i[2] for i in y]
D_dat = [i[3] for i in y]
U_dat = [1 if i <= 1 else 0 for i in t]

plt.plot(t, G_dat)
plt.show()
