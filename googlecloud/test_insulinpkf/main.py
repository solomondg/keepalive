import numpy as np
import matplotlib.pyplot as plt

from simulator import Simulator

time = np.linspace(0, 6000, 10000)
frames = []

dt = time[1] - time[0]

sim = Simulator()
frames.append(sim.get_frame())

for i in range(1, len(time)):
    sim.update(dt)
    frames.append(sim.get_frame())

Y_G = [frame.G for frame in frames]
Y_X = [frame.X for frame in frames]
Y_I = [frame.I for frame in frames]
Y_D = [frame.D for frame in frames]

plt.plot(time, Y_G)
plt.show()