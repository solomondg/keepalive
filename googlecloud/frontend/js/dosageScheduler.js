console.log("Loaded schedule creation plugin");

function processDosageResponse(result) {
    // Can grab any DIV or SPAN HTML element and can then manipulate its
    // contents dynamically via javascript
    console.log("Create response result: " + result);
    var js = JSON.parse(result);

    var httpResult = js["httpCode"];

    console.log("Processing response: " + httpResult);

    if (parseInt(httpResult) === 200) {
        // location.reload();
    } else {
        console.log("Request failed: " + httpResult);
    }
}

function handleDosageClick(e) {
    console.log("Handling create click");

    var form = document.dosageForm;
    var dosage = form.dosage.value;

    console.log("Dosage: " + dosage);

    var data = {};
    data["insulin"] = parseFloat(dosage);
    data["shouldTrigger"] = false;
    data["time"] = Date.now();

    $.post("http://104.196.132.224/insulin", data, function (data) {
        processDosageResponse(data);
    }, 'json').fail(function () {
        processDosageResponse("N/A");
    });

    return false;
}
