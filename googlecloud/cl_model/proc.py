#!/usr/bin/env python3

import random
from rl.policy import BoltzmannQPolicy


import numpy as np

from keras.models import Sequential, Model
from keras.layers import Dense, Activation, Flatten, Input, Concatenate
#from keras.optimizers import Adam

#from rl.agents import DDPGAgent, DQNAgent
#from rl.memory import SequentialMemory
#from rl.policy import BoltzmannQPolicy
#from rl.random import OrnsteinUhlenbeckProcess


class Model:
    def __init__(self):
        self.model = Sequential()
        self.model.add(Flatten(input_shape=(1,12)))
        self.model.add(Dense(32))
        self.model.add(Activation('relu'))
        self.model.add(Dense(32))
        self.model.add(Activation('relu'))
        self.model.add(Dense(32))
        self.model.add(Activation('relu'))
        self.model.add(Dense(6))
        self.model.add(Activation('linear'))
        self.model.load_weights("32model")
        self.model._make_predict_function()
        self.policy = BoltzmannQPolicy()
    def control(self, bgls):
        ret = self.model.predict(np.asarray([bgls]).reshape(1, 1, 12))[0]
        return ret
    
    def dosage(self, bgls):
        return lookup[self.policy.select_action(self.control(bgls))]


lookup = [0, 0.01, 0.03, 0.05, 0.1, 0.25]
from time import sleep
if __name__ == '__main__':
    m = Model()
    #out = m.control([0]*12)
    #print(m.control([0]*12))
    #print(m.dosage([0]*12))
    while True:
        with open('input.csv', 'r') as f:
            incsv = f.read()
        bgl = [float(i) for i in incsv.split(',')]
        dosage = m.dosage(bgl)
        print(dosage)
        with open('output.csv', 'w') as f:
            f.write(str(dosage))
        sleep(15)
